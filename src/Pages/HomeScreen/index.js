import {StyleSheet, Text, View, ScrollView, RefreshControl} from 'react-native';
import React, {useState, useEffect} from 'react';
import Recommend from '../../Component/Recommend';
import LastsUp from '../../Component/LastsUp';
import axios from 'axios';
import Loading from '../../Component/Loading';

const HomeScreen = ({navigation}) => {
  const [refresh, setRefresh] = useState(false);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  let isMount = true;
  async function getData() {
    try {
      await axios
        .get('http://code.aldipee.com/api/v1/movies/')
        .then(response => {
          setData(response.data.results);
        });
    } catch (err) {
      console.error(err);
      setLoading(false);
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    getData();
    return () => {
      let isMount = false;
    };
  });
  const onRefresh = () => {
    setRefresh(true);
    getData();
    setRefresh(false);
  };
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <View style={styles.container}>
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
            }>
            {/* buat komponen recommend */}
            <View style={styles.reco}>
              <Text style={styles.title}>Recommend</Text>
              <ScrollView horizontal>
                <View style={styles.imgreco}>
                  {data.map(i => {
                    return (
                      <Recommend key={i.id} poster={{uri: i.poster_path}} />
                    );
                  })}
                </View>
              </ScrollView>
            </View>

            {/* buat componen lastUp */}
            <View>
              <Text style={styles.title2}>Lasts Update</Text>
              {data.map(i => {
                return (
                  <LastsUp
                    key={i.id}
                    poster={{uri: i.poster_path}}
                    title={i.title}
                    date={i.release_date}
                    rating={i.vote_average}
                    genre={i.id}
                    toDetail={() => navigation.navigate('Detail', {id: i.id})}
                  />
                );
              })}
            </View>
          </ScrollView>
        </View>
      )}
    </>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  imgreco: {
    flexDirection: 'row',
  },
  title: {
    color: 'white',
    backgroundColor: 'black',
    fontWeight: 'bold',
    fontSize: 22,
  },
  title2: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22,
    paddingHorizontal: 12,
    paddingTop: 12,
    backgroundColor: 'black',
  },
  reco: {
    paddingHorizontal: 12,
    paddingTop: 12,
    backgroundColor: 'black',
  },
});
