import { StyleSheet, Text, View, Image } from 'react-native'
import React, {useEffect} from 'react'
import { Logo } from '../../Assets/images'

const SplashScreen = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
          navigation.replace('Home')
        }, 3000)
      }, [navigation])
  return (
    <View style={styles.container }>
        <Image source={Logo} style={styles.Logo}/>
      <Text style={styles.rsp}>Rifhia Selia Putri</Text>
    </View>
  )
}

export default SplashScreen

const styles = StyleSheet.create({
    Logo:{
        height:250,
        width:250,
    },
    container:{
        flex:1,
        backgroundColor:'black',
        justifyContent:'center',
        alignItems:'center',
    },
    rsp:{
        color:'white',
        fontWeight:'bold',
    }
})