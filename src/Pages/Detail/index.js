import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  LogBox,
  FlatList,
  TouchableOpacity,
  ImageBackground,
  RefreshControl,
  Button,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Banner from '../../Component/Banner';
import Genre from '../../Component/Genre';
import Synopsis from '../../Component/Synopsis';
import Actors from '../../Component/Actors';
import axios from 'axios';
import {Back, Like, Share} from '../../Assets/Icons/index';
import {useNavigation} from '@react-navigation/native';
import ShareSocial from 'react-native-share';

function Detail({route}) {
  const [refresh, setRefresh] = useState(false);
  const navigation = useNavigation();
  const [data, setData] = useState({});
  const {id} = route.params;
  let isMount = true;
  async function getData(id) {
    await axios
      .get(`http://code.aldipee.com/api/v1/movies/${id}`)
      .then(response => {
        setData(response.data);
      });
  }
  useEffect(() => {
    getData(id);
    return () => {
      let isMount = false;
    };
  });
  const onRefresh = () => {
    setRefresh(true);
    getData();
    setRefresh(false);
  };
  const myShare = async () => {
    const shareOptions = {
      message: `you can watch this movie ${data.title} ${data.homepage}`,
    };

    try {
      const ShareResponse = await ShareSocial.open(shareOptions);
    } catch (err) {
      console.log('Error => ', err);
    }
  };
  return (
    <SafeAreaView style={styles.chris}>
      <FlatList
        numColumns={3}
        columnWrapperStyle={{marginHorizontal: 20}}
        data={data.credits?.cast}
        keyExtractor={index => index.id}
        refreshControl={
          <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
        }
        ListHeaderComponent={() => (
          <>
            <View style={styles.bg}>
              <ImageBackground
                style={styles.BGImg}
                source={{
                  uri: data.backdrop_path,
                }}>
                <View style={styles.dela}>
                  <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image style={styles.aden} source={Back} />
                  </TouchableOpacity>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity>
                      <Image style={[styles.aden, styles.des]} source={Like} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={myShare}>
                      <Image style={styles.aden} source={Share} />
                    </TouchableOpacity>
                  </View>
                </View>
              </ImageBackground>
              <Banner
                poster={{uri: data.poster_path}}
                title={data.title}
                tagline={data.tagline}
                status={data.status}
                date={data.release_date}
                rating={data.vote_average}
                runtime={data.runtime}
              />
              <Text style={styles.title}>Genre</Text>
              <View style={styles.genre}>
                {data.genres?.map((i)=>(
                  <Genre key={i.id}genre={i.name}/>
                ))}
              </View>
              <Synopsis synopsis={data.overview} />
              <Actors movieData={data} />
            </View>
          </>
        )}
        renderItem={({item}) => (
          <View>
            <Image
              style={styles.Acim}
              source={{
                uri: item.profile_path,
              }}
            />
            <Text style={styles.text}>{item.name}</Text>
          </View>
        )}
      />
    </SafeAreaView>
  );
}

export default Detail;

const styles = StyleSheet.create({
  BGImg: {
    height: 230,
    width: '100%',
    marginRight: 10,
  },
  bg: {
    backgroundColor: 'black',
  },
  chris: {
    flex: 1,
    backgroundColor: 'black',
  },
  Acim: {
    marginTop: 10,
    height: 120,
    width: 90,
    marginRight: 10,
    backgroundColor: 'black',
    marginVertical: 2,
    borderRadius: 3,
  },
  text: {
    flexWrap: 'wrap',
    width: 90,
    textAlign: 'center',
    color: 'black',
    backgroundColor: 'lightgrey',
    marginVertical: 2,
    borderRadius: 3,
    fontWeight: 'bold',
  },
  aden: {
    height: 40,
    width: 40,
  },
  dela: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 12,
    marginTop: 10,
  },
  des: {
    marginRight: 10,
  },
  title:{
    margin: 12,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  genre:{
    flexDirection:'row',
    marginHorizontal: 12,
  }
});
