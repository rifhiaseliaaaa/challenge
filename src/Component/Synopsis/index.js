import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Synopsis = ({synopsis}) => {
  return (
    <View style={styles.sino}>
      <Text style={styles.title}>Synopsis</Text>
      <Text style={{color: 'white', fontSize: 17}}>{synopsis}</Text>
    </View>
  );
};

export default Synopsis;

const styles = StyleSheet.create({
  sino: {
    marginTop: 10,
    marginHorizontal: 15,
  },
  title: {
    marginBottom: 5,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
});
