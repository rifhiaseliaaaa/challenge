import {StyleSheet, Text, View, ScrollView} from 'react-native';
import React from 'react';
import MovieList from '../MovieList';
import Detail from '../../Pages/Detail';

const LastsUp = ({navigation, poster, title, date, rating, genre, toDetail}) => {
  return (
    <View style={styles.container}>
      <MovieList
        poster={poster}
        title={title}
        date={date}
        rating={rating}
        genre={genre}
        toDetail={toDetail}
      />
    </View>
  );
};

export default LastsUp;

const styles = StyleSheet.create({
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 15,
  },
  container: {
    paddingHorizontal: 12,
    backgroundColor: 'black',
  },
});
