import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';

const Genre = ({genre}) => {
 
  return (
    <View style={styles.genre}>
        <TouchableOpacity style={styles.igen}>
          <Text style={{fontSize: 12, fontWeight: 'bold', color: 'black'}}>
            {genre}
          </Text>
        </TouchableOpacity>
    </View>
  );
};

export default Genre;

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 8,
  },
  igen: {
    height: 25,
    width: 55,
    backgroundColor: 'lightgrey',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    marginRight: 5,
    marginTop: 5,
  },
});
