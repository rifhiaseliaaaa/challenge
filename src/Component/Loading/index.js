import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import React from 'react'

const Loading = () => {
  return (
    <View style={styles.load}>
      <ActivityIndicator size={'large'} color={'red'}/>
    </View>
  )
}

export default Loading

const styles = StyleSheet.create({
    load:{
        flex:1,
        backgroundColor:'black',
        justifyContent:'center',
        alignItems:'center',
    }
})