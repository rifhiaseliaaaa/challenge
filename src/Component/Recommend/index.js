import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import React from 'react';

const Recommend = ({poster}) => {
  return (
    <View style={styles.container}>
      <View style={styles.sp}>
        <View style={styles.posters}>
          <Image style={styles.poster} source={poster} />
        </View>
      </View>
    </View>
  );
};

export default Recommend;

const styles = StyleSheet.create({
  container: {
    height: 230,
    backgroundColor: 'black',
  },
  poster: {
    marginTop: 10,
    height: 200,
    width: 130,
    marginRight: -5,
    borderRadius: 7,
  },
  sp: {
    marginHorizontal: 12,
  },
  posters: {
    flexDirection: 'row',
  },
});
