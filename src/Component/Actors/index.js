import {StyleSheet, Text, View, Image, FlatList} from 'react-native';
import React from 'react';

function Actors (props) {
  return (
    <View style={styles.upa}>
      <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
        Actors/Actris
      </Text>
      <View style={styles.imac}>
        <View>
        <FlatList
          columnWrapperStyle={{marginHorizontal:18}}
          numColumns={3}
          data={props.movieData.credits}
          keyExtractor={(item, index) => index}
          renderItem={({item})=>(
            <View>
              <Image
            style={styles.Acim}
            source={{
              uri: item.cast.profile_path,
            }}
          />
          {console.log('hhjhkhk')}
          <Text style={styles.text}>{item.name}</Text>
            </View>
          )}
          />
        </View>
      </View>
    </View>
  );
};

export default Actors;

const styles = StyleSheet.create({
  upa: {
    marginTop: 10,
    marginHorizontal: 15,
    marginBottom: 9,
  },
  imac: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  Acim: {
    marginTop: 10,
    height: 120,
    width: 90,
    marginRight: 10,
    backgroundColor: 'grey',
    marginVertical: 2,
    borderRadius: 3,
  },
  text: {
    flexWrap: 'wrap',
    width: 90,
    textAlign: 'center',
    color: 'black',
    backgroundColor: 'lightgrey',
    marginVertical: 2,
    borderRadius: 3,
    fontWeight: 'bold',
  },
});
