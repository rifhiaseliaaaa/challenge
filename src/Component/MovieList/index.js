import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Button,
} from 'react-native';
import React, {useState, useEffect} from 'react';

import {useNavigation} from '@react-navigation/native';
import Detail from '../../Pages/Detail';
import Genre from '../Genre';
import axios from 'axios';

const MovieList = ({onPress, poster, title, date, rating, genre, toDetail}) => {
  const navigation = useNavigation();
  const [data, setData] = useState([]);
  useEffect(() => {
    let mounted = true;
    getData(genre);
    return () => {
      mounted = false;
    };
    async function getData(genre) {
      try {
        const response = await axios.get(
          `http://code.aldipee.com/api/v1/movies/${genre}`,
        );
        setData(response.data);
      } catch (error) {
        console.error(error);
      }
    }
  });
  
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Image style={styles.poster} source={poster} />
        <View style={styles.box}>
          <Text style={styles.text}>{title}</Text>
          <Text style={styles.text2}>{date}</Text>
          <Text style={styles.text2}>{rating}</Text>
          {/* <Text style={styles.text2}>{genre}</Text> */}
          <View style={styles.genres}>
            {data.genres?.map((i) => (
              <Genre key={i.id} genre={i.name} />
              // <Text style={{color:'white'}} key={i.id}>{i.name}</Text>
            ))}
          </View>
          <TouchableOpacity style={styles.button} onPress={toDetail}>
            <Text style={{fontWeight: 'bold', color: 'black', fontSize: 15, }}>Show More</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default MovieList;
const styles = StyleSheet.create({
  container: {},
  poster: {
    marginTop: 10,
    height: 200,
    width: 130,
    marginRight: 10,
    borderRadius: 7,
  },
  box: {
    margin: 10,
  },
  text: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 20,
    flexWrap: 'wrap',
    width: 199,
  },
  text2: {
    color: 'white',
    fontSize: 15,
  },
  button: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    height: 40,
    width: 80,
  },
  genres:{
    flexDirection:'row',
    flexWrap:'wrap',
    width:220,
    fontSize: 15,
  }
});
