import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';

const Banner = ({poster, title, tagline, status, date, rating, runtime}) => {
  return (
    <View style={styles.Card}>
      <Image
        style={styles.poster}
        source={poster}
      />
      <View style={styles.Data}>
        <Text style={styles.text}>{title}</Text>
        <Text style={styles.text2}> {tagline}</Text>
        <Text style={styles.text2}> {status} </Text>
        <Text style={styles.text2}> {date} </Text>
        <Text style={styles.text2}> {rating} </Text>
        <Text style={styles.text2}> {runtime} </Text>
      </View>
    </View>
  );
};

export default Banner;

const styles = StyleSheet.create({
  poster: {
    height: 170,
    width: 120,
    marginRight: 10,
  },
  Card: {
    marginTop: -90,
    borderWidth: 1,
    backgroundColor: 'lightgrey',
    marginHorizontal: 15,
    flexDirection: 'row',
    borderRadius:7,
  },
  Data: {
    marginTop:8,
    marginLeft:8,
  },
  text:{
    fontWeight: 'bold',
    color: 'black',
    fontSize: 20,
    flexWrap:'wrap',
    width: 180,
  },
  text2:{
    color: 'black',
    fontSize: 15,
    fontWeight: 'bold'
  }
});
