// In App.js in a new project

import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Router from './src/Router';
import NetInfo from '@react-native-community/netinfo';
import NoInternet from './src/Component/NoInternet';

const Stack = createNativeStackNavigator();

function App() {
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
    return () => removeNetInfoSubscription();
  }, []);
  return (
    <>
    {
    isOffline ? <NoInternet/> : <NavigationContainer><Router /></NavigationContainer>
  }
  </>
  );
}

export default App;
